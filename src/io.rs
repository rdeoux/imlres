use std::io::{Read, Result, Write};

pub trait ReadExt {
    fn read_u16(&mut self) -> Result<u16>;
    fn read_u32(&mut self) -> Result<u32>;
}

impl<T: Read> ReadExt for T {
    fn read_u16(&mut self) -> Result<u16> {
        let mut buf = [0; 2];
        self.read_exact(&mut buf).map(|()| u16::from_le_bytes(buf))
    }

    fn read_u32(&mut self) -> Result<u32> {
        let mut buf = [0; 4];
        self.read_exact(&mut buf).map(|()| u32::from_le_bytes(buf))
    }
}

pub trait WriteExt {
    fn write_u16(&mut self, v: u16) -> Result<()>;
    fn write_u32(&mut self, v: u32) -> Result<()>;
}

impl<T: Write> WriteExt for T {
    fn write_u16(&mut self, v: u16) -> Result<()> {
        self.write_all(&v.to_le_bytes())
    }

    fn write_u32(&mut self, v: u32) -> Result<()> {
        self.write_all(&v.to_le_bytes())
    }
}
