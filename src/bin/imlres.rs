#![doc = env!("CARGO_PKG_DESCRIPTION")]
#![warn(clippy::pedantic)]
#![warn(clippy::nursery)]
#![warn(missing_docs)]

use clap::{ArgAction, CommandFactory, Parser, Subcommand};
use clap_complete::{generate, Shell};
use core::mem::size_of;
use imlres::Version;
use log::LevelFilter;
use simple_logger::SimpleLogger;
use std::io;
use std::process::exit;

/// Error type
pub enum Error {
    /// Error returned by the image crate
    Image(image::ImageError),
    /// Error returned by the internal library
    Imlres(imlres::Error),
    /// Invalid font
    InvalidFont,
    /// Invalid resource type
    InvalidResType,
    /// General I/O error
    Io(io::Error),
    /// The glyph or logo to be created is too big to fit in an IML resource
    TooBig,
}

impl Error {
    const fn io_error(&self) -> Option<&io::Error> {
        match self {
            Self::Image(err) => {
                if let image::ImageError::IoError(err) = err {
                    Some(err)
                } else {
                    None
                }
            }
            Self::Imlres(err) => {
                if let imlres::Error::Io(err) = err {
                    Some(err)
                } else {
                    None
                }
            }
            Self::InvalidFont | Self::InvalidResType | Self::TooBig => None,
            Self::Io(err) => Some(err),
        }
    }

    /// Returns the OS-specific error code if any
    pub fn raw_os_error(&self) -> Option<i32> {
        self.io_error().and_then(io::Error::raw_os_error)
    }
}

fn parse_version(s: &str) -> Result<Version, String> {
    if s.as_bytes().len() > size_of::<Version>() {
        Err(format!("{:?} is too long for a version", s))
    } else {
        let s = s.as_bytes();
        let mut version = Version::default();
        version[..s.len()].copy_from_slice(s);
        Ok(version)
    }
}

mod font {
    use super::{parse_version, Error};
    use clap::error::ErrorKind;
    use clap::{command, Args, Subcommand};
    use core::ops::RangeInclusive;
    use drawille::Canvas;
    use image::{DynamicImage, ImageOutputFormat};
    use imlres::{Font, Page, Res, TooBig, Version};
    use indicatif::{ProgressBar, ProgressStyle};
    use log::{debug, error, info};
    use rusttype::{Font as SysFont, Scale};
    use std::fs::{read, File};
    use std::io::{self, stdout, BufWriter, Cursor, Seek, Write};
    use std::path::{Path, PathBuf};
    use std::str::FromStr;

    #[derive(Clone)]
    pub enum Size {
        Px(f32),
        Mm(f32),
        Pt(f32),
    }

    impl Size {
        fn into_px(self) -> f32 {
            match self {
                Self::Px(val) => val,
                Self::Mm(val) => val * 8.0,
                Self::Pt(val) => val * 254.0 / 90.0,
            }
        }
    }

    impl FromStr for Size {
        type Err = clap::Error;

        fn from_str(s: &str) -> Result<Self, Self::Err> {
            if s.len() > 2 {
                let (val, unit) = s.split_at(s.len() - 2);
                if let Ok(val) = val.parse() {
                    match unit {
                        "px" => return Ok(Self::Px(val)),
                        "mm" => return Ok(Self::Mm(val)),
                        "pt" => return Ok(Self::Pt(val)),
                        _ => {}
                    }
                }
            }
            Err(clap::Error::new(ErrorKind::ValueValidation))
        }
    }

    #[derive(Clone)]
    pub struct Codepoint(pub char);

    impl FromStr for Codepoint {
        type Err = clap::Error;

        fn from_str(s: &str) -> Result<Self, Self::Err> {
            let codepoint: u32 = if let Some(s) = s.strip_prefix("0x") {
                u32::from_str_radix(s, 16)
            } else if let Some(s) = s.strip_prefix('0') {
                u32::from_str_radix(s, 8)
            } else {
                s.parse::<u32>()
            }
            .map_err(|err| clap::Error::raw(ErrorKind::ValueValidation, err))?;
            char::try_from(codepoint)
                .map(Self)
                .map_err(|err| clap::Error::raw(ErrorKind::ValueValidation, err))
        }
    }

    fn save_iml_font(font: Font, path: &Path) -> io::Result<()> {
        info!("Save {:?}", path);
        let writer = File::create(path).map_err(|err| {
            error!("Failed to open {:?}: {}", path, err);
            err
        })?;
        let mut writer = BufWriter::new(writer);
        let res = Res::Font(font);
        let pb = ProgressBar::new(res.size() as u64)
            .with_style(
                ProgressStyle::default_bar()
                    .template("{msg} {wide_bar} {elapsed}/{duration}")
                    .unwrap_or_else(|err| {
                        error!("invalid templeate: {}", err);
                        ProgressStyle::default_bar()
                    }),
            )
            .with_message("Saving font");
        res.to_writer(pb.wrap_write(&mut writer)).map_err(|err| {
            error!("Failed to save font: {}", err);
            err
        })?;
        writer.flush()
    }

    fn load_iml_font(path: &Path) -> Result<Font, Error> {
        info!("Load IML font {:?}", path);
        let reader = File::open(path).map_err(|err| {
            error!("Failed to open {:?}: {}", path, err);
            Error::Io(err)
        })?;
        let res = Res::from_reader(reader).map_err(|err| {
            error!("Failed to load {:?}: {}", path, err);
            Error::Imlres(err)
        })?;
        if let Res::Font(font) = res {
            Ok(font)
        } else {
            error!("{:?} is not an IML font but an IML logo", path);
            Err(Error::InvalidResType)
        }
    }

    #[derive(Args)]
    pub struct New {
        #[arg(value_parser = parse_version)]
        /// Version of the font (up to 12 characters)
        pub version: Version,

        #[arg(requires = "size")]
        /// Path to an existing system font
        pub font_file: Option<PathBuf>,

        #[arg(value_parser = Size::from_str)]
        /// Size of the font to render (eg: 12mm, 12pt or 12px)
        pub size: Option<Size>,

        #[arg(long, requires = "font_file")]
        /// Imports only the ASCII symbols
        pub ascii: bool,
    }

    impl New {
        fn run(self, iml_font: &Path) -> Result<(), Error> {
            debug!("version: {:?}", String::from_utf8_lossy(&self.version));
            let mut font = Font::from(self.version);
            if let Some(sys_font_path) = self.font_file {
                debug!("font file: {:?}", sys_font_path);
                debug!("ascii: {}", self.ascii);
                let size_px = self.size.unwrap().into_px();
                debug!("size: {}px", size_px);
                info!("Load system font {:?}", sys_font_path);
                let sys_font_bytes = read(sys_font_path).map_err(|err| {
                    error!("Failed to load {:?}: {}", font, err);
                    Error::Io(err)
                })?;
                let sys_font = SysFont::try_from_bytes(&sys_font_bytes)
                    .ok_or(Error::InvalidFont)
                    .map_err(|err| {
                        error!("{:?} is not a valid font", sys_font_bytes);
                        err
                    })?;
                let scale = Scale::uniform(size_px);
                if self.ascii {
                    info!("Import glyphs (ASCII-only)");
                    font.pages
                        .entry(0)
                        .or_insert_with(Page::default)
                        .import_system_font(&sys_font, scale, 0, 0..std::u8::MAX)
                        .map_err(|TooBig| {
                            error!("Failed to import font: too big");
                            Error::TooBig
                        })?;
                } else {
                    const FULL: RangeInclusive<char> = '0'..='\u{ffff}';
                    info!("Import glyphs (full)");
                    let pb = ProgressBar::new(FULL.count() as u64)
                        .with_style(
                            ProgressStyle::default_bar()
                                .template("{msg} {wide_bar} {elapsed}/{duration}")
                                .unwrap_or_else(|err| {
                                    error!("Invalid template: {}", err);
                                    ProgressStyle::default_bar()
                                }),
                        )
                        .with_message("Importing glyphs");
                    font.import_system_font(&sys_font, scale, pb.wrap_iter(FULL))
                        .map_err(|TooBig| {
                            error!("Faile to import font: too big");
                            Error::TooBig
                        })?;
                }
                font.pack();
            }
            save_iml_font(font, iml_font).map_err(Error::Io)?;
            info!("IML font successfully created!");
            Ok(())
        }
    }

    #[derive(Args)]
    pub struct Add {
        /// Path to an existing system font
        pub font_file: PathBuf,

        /// Size of the font to render (eg: 12mm, 12pt or 12px)
        pub size: Size,

        /// Unicode code point of the symbol to insert or replace
        pub code_point: Codepoint,
    }

    impl Add {
        fn run(self, path: &Path) -> Result<(), Error> {
            let font = &self.font_file;
            debug!("font file: {:?}", font);
            let size_px = self.size.into_px();
            debug!("size: {}px", size_px);
            let code_point = self.code_point.0;
            info!("Load system font {:?}", font);
            let sys_font = read(font).map_err(|err| {
                error!("Failed to load {:?}: {}", font, err);
                Error::Io(err)
            })?;
            let sys_font = SysFont::try_from_bytes(&sys_font).ok_or_else(|| {
                error!("{:?} is not a valid font", sys_font);
                Error::InvalidFont
            })?;
            let mut iml_font = load_iml_font(path)?;
            iml_font
                .import_system_font(&sys_font, Scale::uniform(size_px), Some(code_point))
                .map_err(|TooBig| {
                    error!(
                        "Failed to import glyph {:?} ({}px): too big",
                        code_point, size_px
                    );
                    Error::TooBig
                })?;
            save_iml_font(iml_font, path).map_err(Error::Io)
        }
    }

    #[derive(Args)]
    pub struct Rm {
        /// Unicode code point of the symbol to be removed
        pub code_point: Codepoint,
    }

    impl Rm {
        fn run(self, path: &Path) -> Result<(), Error> {
            let code_point = self.code_point.0;
            let mut iml_font = load_iml_font(path)?;
            info!("Remove symbol {:#x} ({:?})", code_point as u32, code_point);
            let code_point = (code_point as u32).to_le_bytes();
            let cell = code_point[0];
            let row = code_point[1];
            if let Some(page) = iml_font.pages.get_mut(&row) {
                let mut glyph = page.glyph_mut(cell);
                *glyph.advance_mut() = 0;
                glyph.clear();
            }
            save_iml_font(iml_font, path).map_err(Error::Io)
        }
    }

    #[derive(Args)]
    pub struct Ls {
        #[arg(short, long)]
        /// Long display
        pub long: bool,
    }

    impl Ls {
        fn run(self, iml_font: &Path) -> Result<(), Error> {
            let font = load_iml_font(iml_font)?;
            if self.long {
                println!("   Oct    Dec   Hex  Char  Width  Height  Advance");
                for _ in 0..49 {
                    print!("\u{2500}");
                }
                println!();
            }
            for (row, page) in font.pages {
                let row = u16::from(row) << 8;
                for cell in 0..=std::u8::MAX {
                    let glyph = page.glyph(cell);
                    if glyph.advance() > 0 && glyph.as_slice().iter().any(|w| *w != 0) {
                        let c = row | u16::from(cell);
                        if self.long {
                            println!(
                                "{:6o}  {0:5}  {0:4x}  {:4}  {:5}  {:6}  {:7}",
                                c,
                                char::try_from(u32::from(c))
                                    .map_or_else(|_| format!("\\u{{{:x}}}", c), |x| x.to_string()),
                                page.width(),
                                page.height(),
                                glyph.advance()
                            );
                        } else {
                            println!("{}", c);
                        }
                    }
                }
            }
            Ok(())
        }
    }

    #[derive(Args)]
    pub struct Cat {
        #[arg(long)]
        /// Extracts to Bitmap
        pub bmp: bool,

        #[arg(long, conflicts_with = "bmp")]
        /// Extracts to PNG
        pub png: bool,

        /// Unicode code point of the symbol to extract
        pub code_point: Codepoint,

        /// Output file path
        pub output: Option<PathBuf>,
    }

    impl Cat {
        fn run_output<T: Write + Seek>(self, path: &Path, mut output: T) -> Result<(), Error> {
            let font = load_iml_font(path)?;
            let codepoint = (self.code_point.0 as u32).to_le_bytes();
            if let Some(page) = font.pages.get(&codepoint[1]) {
                let glyph = page.glyph(codepoint[0]);
                if self.bmp || self.png {
                    let im = glyph.to_gray_image();
                    let im = DynamicImage::ImageLuma8(im);
                    im.write_to(
                        &mut output,
                        if self.bmp {
                            ImageOutputFormat::Bmp
                        } else {
                            ImageOutputFormat::Png
                        },
                    )
                    .map_err(|err| {
                        error!("Failed to export glyph: {}", err);
                        Error::Image(err)
                    })
                } else {
                    let mut canvas = Canvas::new(page.width().into(), page.height().into());
                    for x in 0..page.width() {
                        for y in 0..page.height() {
                            if glyph.get(x, y) == Some(true) {
                                canvas.set(x.into(), y.into());
                            }
                        }
                    }
                    writeln!(output, "{}", canvas.frame()).map_err(|err| {
                        error!("Failed to export glyph: {}", err);
                        Error::Io(err)
                    })
                }
            } else {
                Ok(())
            }
        }

        fn run(mut self, path: &Path) -> Result<(), Error> {
            match self.output.take().as_ref() {
                None => {
                    let mut output = Cursor::new(Vec::new());
                    self.run_output(path, &mut output)?;
                    stdout().write_all(&output.into_inner()).map_err(Error::Io)
                }
                Some(path) => {
                    let output = File::create(path).map_err(|err| {
                        error!("Failed to create {:?}: {}", path, err);
                        Error::Io(err)
                    })?;
                    self.run_output(path, output)
                }
            }
        }
    }

    #[derive(Subcommand)]
    pub enum Commands {
        /// Creates a new IML font
        New(New),
        /// Imports a glyph from a system font
        Add(Add),
        /// Remove a character
        Rm(Rm),
        /// Lists symbols
        Ls(Ls),
        /// Extracts a character
        Cat(Cat),
    }

    impl Commands {
        fn run(self, iml_font: &Path) -> Result<(), Error> {
            match self {
                Self::New(command) => command.run(iml_font),
                Self::Add(command) => command.run(iml_font),
                Self::Rm(command) => command.run(iml_font),
                Self::Ls(command) => command.run(iml_font),
                Self::Cat(command) => command.run(iml_font),
            }
        }
    }

    #[derive(Args)]
    /// Creates, inspect and modifies IML fonts
    pub struct Cli {
        /// Path to an IML font
        pub iml_font: PathBuf,

        #[command(subcommand)]
        pub command: Commands,
    }

    impl Cli {
        pub fn run(self) -> Result<(), Error> {
            debug!("IML font path: {:?}", self.iml_font);
            self.command.run(&self.iml_font)
        }
    }
}

mod logo {
    use super::{parse_version, Error};
    use clap::{Args, Subcommand};
    use image::{open, GrayImage, Luma};
    use imlres::{self, Logo, Res, Version};
    use log::{debug, error, info};
    use std::fs::File;
    use std::path::{Path, PathBuf};

    #[derive(Args)]
    pub struct CliLogoFrom {
        /// Path to an existing image
        pub image: PathBuf,

        #[arg(value_parser = parse_version)]
        /// Version of the logo (up to 12 characters)
        pub version: Version,
    }

    impl CliLogoFrom {
        fn run(self, iml_logo_path: &Path) -> Result<(), Error> {
            debug!("image path: {:?}", &self.image);
            debug!("version: {}", String::from_utf8_lossy(&self.version));
            let image = open(&self.image)
                .map_err(|err| {
                    error!("Failed to open image {:?}: {}", self.image, err);
                    Error::Image(err)
                })?
                .into_luma8();
            if let (Ok(width), Ok(height)) = (image.width().try_into(), image.height().try_into()) {
                let mut logo = Logo::new(self.version, width, height);
                for x in 0..width {
                    for y in 0..height {
                        if image.get_pixel(x.into(), y.into()).0[0] < 0x80 {
                            logo.set(x, y, true);
                        }
                    }
                }

                File::create(iml_logo_path)
                    .and_then(|writer| Res::Logo(logo).to_writer(writer))
                    .map_err(|err| {
                        error!("Failed to create logo: {}", err);
                        Error::Io(err)
                    })
                    .map(|()| info!("IML logo successfully created!"))
            } else {
                error!(
                    "The image is too big: {}\u{d7}{}",
                    image.width(),
                    image.height()
                );
                Err(Error::TooBig)
            }
        }
    }

    #[derive(Args)]
    pub struct CliLogoTo {
        /// Path of the resulting image
        pub image: PathBuf,
    }

    impl CliLogoTo {
        fn run(self, iml_logo_path: &Path) -> Result<(), Error> {
            debug!("image path: {:?}", self.image);
            let reader = File::open(iml_logo_path).map_err(|err| {
                error!("Failed to open {:?}: {}", iml_logo_path, err);
                Error::Io(err)
            })?;
            let res = Res::from_reader(reader).map_err(|err| {
                error!("Failed to load IML logo: {}", err);
                Error::Imlres(err)
            })?;
            match res {
                Res::Font(..) => {
                    error!("{:?} is a font, not a logo", iml_logo_path);
                    Err(Error::InvalidResType)
                }
                Res::Logo(logo) => {
                    let image =
                        GrayImage::from_fn(logo.width().into(), logo.height().into(), |x, y| {
                            Luma([if let (Ok(x), Ok(y)) = (x.try_into(), y.try_into()) {
                                if logo.get(x, y).unwrap_or_default() {
                                    0
                                } else {
                                    0xff
                                }
                            } else {
                                0
                            }])
                        });
                    image.save(self.image).map_err(|err| {
                        error!("Failed to save image: {}", err);
                        Error::Image(err)
                    })
                }
            }
        }
    }

    #[derive(Subcommand)]
    pub enum Commands {
        /// Convert an image into an IML logo
        From(CliLogoFrom),
        /// Convert an IML logo into an image
        To(CliLogoTo),
    }

    #[derive(Args)]
    /// Convert IML logos to and from images
    pub struct Cli {
        pub iml_logo: PathBuf,

        #[command(subcommand)]
        pub command: Commands,
    }

    impl Cli {
        pub fn run(self) -> Result<(), Error> {
            let iml_logo = &self.iml_logo;
            debug!("IML logo path: {:?}", iml_logo);
            match self.command {
                Commands::From(command) => command.run(iml_logo),
                Commands::To(command) => command.run(iml_logo),
            }
        }
    }
}

fn main() {
    #[derive(Subcommand)]
    enum Commands {
        Font(font::Cli),
        Logo(logo::Cli),
        /// Provide shell completion
        Complete { shell: Shell },
    }

    impl Commands {
        fn run(self) -> Result<(), Error> {
            match self {
                Self::Font(args) => args.run(),
                Self::Logo(args) => args.run(),
                Self::Complete { shell } => {
                    let mut cmd = Cli::command();
                    let mut buf = io::stdout();
                    generate(shell, &mut cmd, "imlres", &mut buf);
                    Ok(())
                }
            }
        }
    }

    #[derive(Parser)]
    #[command(author, version)]
    /// Create, inspect and modify IML 5 resources
    struct Cli {
        #[arg(short, long, action = ArgAction::Count)]
        /// Increase verbosity
        pub verbose: u8,

        #[command(subcommand)]
        pub command: Commands,
    }

    impl Cli {
        fn run(self) -> Result<(), Error> {
            SimpleLogger::new()
                .with_level(match self.verbose {
                    0 => LevelFilter::Error,
                    1 => LevelFilter::Warn,
                    2 => LevelFilter::Info,
                    3 => LevelFilter::Debug,
                    _ => LevelFilter::Trace,
                })
                .init()
                .ok();

            self.command.run()
        }
    }

    if let Err(err) = Cli::parse().run() {
        exit(err.raw_os_error().unwrap_or(1))
    }
}
