use crate::{Error, Font, Logo, Version};
use log::debug;
use std::io::{self, Read, Write};

/// IML resource
#[derive(Clone, Debug, Eq, PartialEq)]
pub enum Res {
    /// IML font
    Font(Font),
    /// IML logo
    Logo(Logo),
}

impl Res {
    fn escape(bytes: &[u8]) -> String {
        bytes
            .iter()
            .copied()
            .flat_map(std::ascii::escape_default)
            .map(char::from)
            .collect::<String>()
    }

    /// Load an IML5 resource.
    ///
    /// # Errors
    ///
    /// If the input source is truncated, an I/O error is returned.
    /// If the input source is not a valid resource, an error is returned.
    pub fn from_reader<T: Read>(reader: T) -> Result<Self, Error> {
        let mut reader = reader;

        let mut version = Version::default();
        reader.read_exact(&mut version)?;
        debug!("read version \u{201c}{}\u{201d}", Self::escape(&version));

        let mut magic = [0; 4];
        reader.read_exact(&mut magic)?;
        debug!("read magic bytes \u{201c}{}\u{201d}", Self::escape(&magic));
        match magic {
            Logo::MAGIC => Logo::from_reader(reader, version).map(Self::Logo),
            Font::MAGIC => Font::from_reader(reader, version).map(Self::Font),
            _ => Err(Error::InvalidMagic(magic)),
        }
    }

    /// Load an IML5 resource from a byte array.
    ///
    /// # Errors
    ///
    /// If the data are truncated, an I/O error is returned.
    /// If the data do not hold a valid resource, an error is returned.
    pub fn from_bytes(bytes: &[u8]) -> Result<Self, Error> {
        Self::from_reader(bytes)
    }

    /// Get the binary size of the resource
    #[must_use]
    pub fn size(&self) -> usize {
        match self {
            Self::Font(font) => font.version.len() + Font::MAGIC.len() + font.size(),
            Self::Logo(logo) => logo.version.len() + Logo::MAGIC.len() + logo.size(),
        }
    }

    /// Save an IML5 resource.
    ///
    /// # Errors
    ///
    /// An error may be returned if the writer triggers an error.
    pub fn to_writer<T: Write>(&self, writer: T) -> io::Result<()> {
        let mut writer = writer;
        match self {
            Self::Font(font) => {
                debug!(
                    "write version \u{201c}{}\u{201d}",
                    Self::escape(&font.version)
                );
                writer.write_all(&font.version)?;
                debug!(
                    "write magic bytes \u{201c}{}\u{201d}",
                    Self::escape(&Font::MAGIC)
                );
                writer.write_all(&Font::MAGIC)?;
                font.to_writer(writer)
            }
            Self::Logo(logo) => {
                debug!(
                    "write version \u{201c}{}\u{201d}",
                    Self::escape(&logo.version)
                );
                writer.write_all(&logo.version)?;
                debug!(
                    "write magic bytes \u{201c}{}\u{201d}",
                    Self::escape(&Logo::MAGIC)
                );
                writer.write_all(&Logo::MAGIC)?;
                logo.to_writer(writer)
            }
        }
    }

    /// Convert an IML resource into a byte array
    #[must_use]
    pub fn to_bytes(&self) -> Vec<u8> {
        let mut vec = Vec::new();
        vec.reserve(self.size());
        self.to_writer(&mut vec).ok(); // writing to a Vec never fails
        vec
    }
}
