use crate::{Error, Image, ReadExt, Version, WriteExt};
use log::debug;
use std::io::{self, Read, Write};

/// IML logo
#[derive(Clone, Debug, Eq, PartialEq)]
pub struct Logo {
    /// Version of the logo
    pub version: Version,
    image: Image,
}

impl Logo {
    /// Expected magic bytes
    pub const MAGIC: [u8; 4] = *b"OGOL";

    /// Create a new logo
    #[must_use]
    pub fn new(version: Version, width: u16, height: u16) -> Self {
        let image = Image::new(width, height);
        Self { version, image }
    }

    /// Get the width of the logo
    #[inline]
    #[must_use]
    pub const fn width(&self) -> u16 {
        self.image.width()
    }

    /// Get the height of the logo
    #[inline]
    #[must_use]
    pub const fn height(&self) -> u16 {
        self.image.height()
    }

    /// Change the width of the logo
    #[inline]
    pub fn set_width(&mut self, width: u16) {
        self.image.set_width(width);
    }

    /// Change the height of the logo
    #[inline]
    pub fn set_height(&mut self, height: u16) {
        self.image.set_height(height);
    }

    /// Get the value of a pixel in the logo
    ///
    /// # Example
    ///
    /// ```
    /// # use imlres::{Logo, Version};
    /// # let logo = Logo::new(Version::default(), 0, 0);
    /// # let (x, y) = (0, 0);
    /// match logo.get(x, y) {
    ///   None => println!("The pixel is out of bounds."),
    ///   Some(false) => println!("The pixel is white."),
    ///   Some(true) => println!("The pixel is black."),
    /// }
    /// ```
    #[inline]
    #[must_use]
    pub fn get(&self, x: u16, y: u16) -> Option<bool> {
        self.image.get(x, y)
    }

    /// Set the value of a pixel in the logo
    ///
    /// Out-of-bounds pixels are silently ignored.
    #[inline]
    pub fn set(&mut self, x: u16, y: u16, black: bool) {
        self.image.set(x, y, black);
    }

    pub(crate) fn from_reader<T: Read>(reader: T, version: Version) -> Result<Self, Error> {
        let mut reader = reader;
        debug!("read logo header");

        let width = reader.read_u16()?;
        debug!("  width = {} {0:#x}", width);

        let height = reader.read_u16()?;
        debug!("  height = {} {0:#x}", height);

        {
            let char_size = reader.read_u16()?;
            debug!("  char size = {} {0:#x}", char_size);
            let column_size = (height + 15) / 16;
            if char_size != column_size * width {
                return Err(Error::IncoherentHeader(format!(
                    "column_size = {}, width = {}, char_size = {} (should be {})",
                    column_size,
                    width,
                    char_size,
                    column_size * width
                )));
            }
        }

        {
            let column_size = reader.read_u16()?;
            debug!("  column size = {} {0:#x}", column_size);
            if column_size != (height + 15) / 16 {
                return Err(Error::IncoherentHeader(format!(
                    "height = {}, column_size = {} (should be {})",
                    height,
                    column_size,
                    (height + 15) / 16
                )));
            }
        }

        let image = Image::from_reader(reader, width, height)?;
        Ok(Self { version, image })
    }

    pub(crate) const fn size(&self) -> usize {
        8 + self.image.size()
    }

    pub(crate) fn to_writer<T: Write>(&self, writer: T) -> io::Result<()> {
        let mut writer = writer;

        let width = self.image.width();
        writer.write_u16(width)?;

        let height = self.image.height();
        writer.write_u16(height)?;

        let column_size = self.image.column_size();
        writer.write_u16(width * column_size)?;
        writer.write_u16(column_size)?;
        self.image.to_writer(writer)
    }
}
