use core::fmt::{self, Display, Formatter};
use std::io;

/// Error type
#[derive(Debug)]
pub enum Error {
    /// I/O error
    Io(io::Error),
    /// Invalid magic bytes
    InvalidMagic([u8; 4]),
    /// Incoherent file header (Is it really an IML resource?)
    IncoherentHeader(String),
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        match self {
            Self::Io(err) => write!(f, "I/O error: {}", err),
            Self::InvalidMagic(magic) => write!(f, "Invalid magic bytes: {:?}", magic),
            Self::IncoherentHeader(msg) => write!(f, "Incoherent header: {}", msg),
        }
    }
}

impl From<io::Error> for Error {
    fn from(err: io::Error) -> Self {
        Self::Io(err)
    }
}
