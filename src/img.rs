use crate::{ReadExt, WriteExt};
use log::trace;
use std::io::{self, Read, Write};

#[derive(Clone, Debug, Default, Eq, PartialEq)]
pub struct Image {
    width: u16,
    height: u16,
    bits: Vec<u16>,
}

impl Image {
    #[must_use]
    pub fn new(width: u16, height: u16) -> Self {
        trace!("Image::new({}, {})", width, height);
        let column_size = (height + 15) / 16;
        Self {
            width,
            height,
            bits: vec![0; usize::from(width) * usize::from(column_size)],
        }
    }

    #[inline]
    #[must_use]
    pub const fn width(&self) -> u16 {
        self.width
    }

    #[inline]
    #[must_use]
    pub const fn height(&self) -> u16 {
        self.height
    }

    #[must_use]
    pub const fn column_size(&self) -> u16 {
        (self.height + 15) / 16
    }

    pub fn set_width(&mut self, width: u16) {
        trace!("Image::set_width({})", width);
        if self.width != width {
            self.width = width;
            let new_len = usize::from(self.column_size()) * usize::from(width);
            self.bits.resize(new_len, 0);
        }
    }

    pub fn set_height(&mut self, height: u16) {
        trace!("Image::set_height({})", height);
        if self.height & !15 == height & !15 {
            self.height = height;
        } else if self.height == 0 && height > 0 {
            self.height = height;
            if self.width > 0 {
                let new_len = usize::from(self.column_size()) * usize::from(self.width);
                self.bits.resize(new_len, 0);
            }
        } else {
            todo!()
        }
    }

    #[must_use]
    fn index(&self, x: u16, y: u16) -> Option<(usize, u16)> {
        if y < self.height {
            let y = self.height - 1 - y;
            let index = usize::from(self.column_size()) * usize::from(x) + usize::from(y / 16);
            self.bits.get(index).map(|_| (index, 1 << (y % 16)))
        } else {
            None
        }
    }

    /// Get a pixel at the specified (x, y) coordonate.
    ///
    /// (0, 0) is the top-left corner of the image.
    /// `None` is returned when the coordinate is out of the image.
    #[must_use]
    pub fn get(&self, x: u16, y: u16) -> Option<bool> {
        self.index(x, y)
            .and_then(|(idx, mask)| self.bits.get(idx).map(|word| word & mask != 0))
    }

    /// Set a pixel at the specified (x, y) coordonate.
    ///
    /// (0, 0) is the top-left corner of the image.
    pub fn set(&mut self, x: u16, y: u16, black: bool) {
        if let Some((idx, mask)) = self.index(x, y) {
            if let Some(word) = self.bits.get_mut(idx) {
                if black {
                    *word |= mask;
                } else {
                    *word &= !mask;
                }
            }
        }
    }

    #[must_use]
    pub fn as_slice(&self) -> &[u16] {
        &self.bits
    }

    #[must_use]
    pub fn as_mut_slice(&mut self) -> &mut [u16] {
        &mut self.bits
    }

    pub fn from_reader<T: Read>(reader: T, width: u16, height: u16) -> io::Result<Self> {
        let mut reader = reader;
        let mut buffer = Self::new(width, height);
        for word in &mut buffer.bits {
            *word = reader.read_u16()?;
        }
        Ok(buffer)
    }

    pub const fn size(&self) -> usize {
        (self.width() as usize) * (self.column_size() as usize) * 2
    }

    pub fn to_writer<T: Write>(&self, writer: T) -> io::Result<()> {
        let mut writer = writer;
        for word in &self.bits {
            writer.write_u16(*word)?;
        }
        Ok(())
    }
}
