#![warn(clippy::pedantic)]
#![warn(clippy::nursery)]
#![warn(missing_docs)]

//! This crate provide an API to handle IML resources.
//! It also provide a binary to deal with IML resource files.

extern crate image;
extern crate log;
extern crate rusttype;

mod error;
mod font;
mod img;
mod io;
mod logo;
mod res;

use img::Image;
use io::{ReadExt, WriteExt};

pub use error::Error;
pub use font::{Font, Page, TooBig};
pub use logo::Logo;
pub use res::Res;

/// IML resource version
pub type Version = [u8; 12];
