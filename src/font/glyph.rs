use crate::Page;
use image::{GrayImage, Luma};

#[derive(Debug)]
pub struct Glyph<'a> {
    page: &'a Page,
    cell: u8,
}

impl<'a> Glyph<'a> {
    #[must_use]
    pub const fn new(page: &'a Page, cell: u8) -> Self {
        Self { page, cell }
    }

    pub const fn page(&self) -> &'a Page {
        self.page
    }

    pub const fn cell(&self) -> u8 {
        self.cell
    }

    #[must_use]
    pub const fn advance(&self) -> u8 {
        self.page.advances[self.cell as usize]
    }

    #[must_use]
    pub fn get(&self, x: u8, y: u16) -> Option<bool> {
        let offset = u16::from(self.cell) * u16::from(self.page.width());
        self.page.image.get(offset + u16::from(x), y)
    }

    #[must_use]
    pub fn as_slice(&self) -> &[u16] {
        let char_size = u16::from(self.page.width()) * self.page.image.column_size();
        let char_size = char_size.into();
        &self.page.image.as_slice()[char_size * usize::from(self.cell)..][..char_size]
    }

    pub fn to_gray_image(&self) -> GrayImage {
        GrayImage::from_fn(
            self.page.width().into(),
            self.page.height().into(),
            |x, y| {
                Luma([if let (Ok(x), Ok(y)) = (x.try_into(), y.try_into()) {
                    if self.get(x, y) == Some(true) {
                        255
                    } else {
                        0
                    }
                } else {
                    0
                }])
            },
        )
    }
}

#[derive(Debug)]
pub struct Mut<'a> {
    page: &'a mut Page,
    cell: u8,
}

impl<'a> Mut<'a> {
    #[must_use]
    pub fn new(page: &'a mut Page, cell: u8) -> Self {
        Self { page, cell }
    }

    #[must_use]
    pub const fn advance(&self) -> u8 {
        self.page.advances[self.cell as usize]
    }

    #[must_use]
    pub fn advance_mut(&mut self) -> &mut u8 {
        &mut self.page.advances[usize::from(self.cell)]
    }

    #[inline]
    #[must_use]
    fn offset(&self) -> u16 {
        u16::from(self.cell) * u16::from(self.page.width())
    }

    #[must_use]
    pub fn get(&self, x: u8, y: u16) -> Option<bool> {
        self.page.image.get(self.offset() + u16::from(x), y)
    }

    pub fn set(&mut self, x: u8, y: u16, black: bool) {
        self.page.image.set(self.offset() + u16::from(x), y, black);
    }

    #[must_use]
    pub fn as_mut_slice(&mut self) -> &mut [u16] {
        let char_size = u16::from(self.page.width()) * self.page.image.column_size();
        let char_size = char_size.into();
        &mut self.page.image.as_mut_slice()[char_size * usize::from(self.cell)..][..char_size]
    }

    pub fn clear(&mut self) {
        for word in self.as_mut_slice() {
            *word = 0;
        }
    }
}
