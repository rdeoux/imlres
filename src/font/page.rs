use super::{u16_from_f32, Glyph, GlyphMut, TooBig};
use crate::{Error, Image, ReadExt, WriteExt};
use log::{debug, trace};
use rusttype::{point, Font, Scale};
use std::io::{self, Read, Write};

macro_rules! checked_cast {
    ($e:expr, $t:ident) => {{
        let v = $e;
        debug_assert!(v <= std::$t::MAX.into());
        #[allow(clippy::cast_possible_truncation)]
        {
            v as $t
        }
    }};
}

/// Page of an IML font
#[derive(Clone, Debug, Eq, PartialEq)]
pub struct Page {
    pub(crate) image: Image,
    pub(crate) advances: [u8; 0x100],
}

impl Page {
    /// Create a new page
    ///
    /// `width` and `height` are the maximum dimension of a glyph on that page.
    #[must_use]
    pub fn new(width: u8, height: u16) -> Self {
        Self {
            image: Image::new(u16::from(width) * 0x100, height),
            advances: [0; 0x100],
        }
    }

    /// Get the maximum width of a glyph on that page.
    #[must_use]
    pub const fn width(&self) -> u8 {
        #[allow(clippy::cast_possible_truncation)]
        {
            (self.image.width() / 0x100) as u8
        }
    }

    /// Change the maximum width of a glyph on that page.
    pub fn set_width(&mut self, width: u8) {
        trace!("Page::set_width({})", width);
        let w = self.width();
        if width != w {
            let column_size = usize::from(self.image.column_size());
            let old_char_size = column_size * usize::from(w);
            let new_char_size = column_size * usize::from(width);
            if width > w {
                self.image.set_width(u16::from(width) * 0x100);
                let s = self.image.as_mut_slice();
                for cell in 0..=std::u8::MAX {
                    let cell = usize::from(std::u8::MAX - cell);
                    for i in 0..old_char_size {
                        s[new_char_size * cell + i] = s[old_char_size * cell + i];
                        s[old_char_size * cell + i] = 0;
                    }
                }
            } else {
                let s = self.image.as_mut_slice();
                for cell in 0..=std::u8::MAX.into() {
                    for i in 0..old_char_size {
                        s[new_char_size * cell + i] = s[old_char_size * cell + i];
                    }
                }
                self.image.set_width(u16::from(width) * 0x100);
            }
        }
    }

    /// Get the maximum height of a glyph on that page.
    #[must_use]
    pub const fn height(&self) -> u16 {
        self.image.height()
    }

    /// Get a glyph.
    #[must_use]
    pub const fn glyph(&self, cell: u8) -> Glyph {
        Glyph::new(self, cell)
    }

    /// Get a mutable glyph.
    pub fn glyph_mut(&mut self, cell: u8) -> GlyphMut {
        GlyphMut::new(self, cell)
    }

    /// Import glyphs from a system font
    ///
    /// # Errors
    ///
    /// Returns an error if the rendered font is too large to fit in an IML font.
    pub fn import_system_font<I>(
        &mut self,
        font: &Font,
        scale: Scale,
        row: u8,
        cells: I,
    ) -> Result<(), TooBig>
    where
        I: IntoIterator<Item = u8>,
    {
        let v_metrics = font.v_metrics(scale);
        let height = u16_from_f32(v_metrics.ascent - v_metrics.descent).ok_or(TooBig)?;
        if height > self.image.height() {
            self.image.set_height(height);
        }
        for cell in cells {
            let c = u32::from_le_bytes([cell, row, 0, 0]);
            if let Ok(c) = char::try_from(c) {
                let glyph = font.glyph(c);
                if glyph.id().0 > 0 {
                    debug!("import glyph {:?}", c);
                    let glyph = glyph.scaled(scale);
                    {
                        let mut g = self.glyph_mut(cell);
                        *g.advance_mut() = u16_from_f32(glyph.h_metrics().advance_width)
                            .ok_or(TooBig)?
                            .try_into()
                            .map_err(|_err| TooBig)?;
                        g.clear();
                    }
                    let glyph = glyph.positioned(point(0.0, v_metrics.ascent));
                    if let Some(rect) = glyph.pixel_bounding_box() {
                        if rect.max.x >= 0 {
                            let width = rect.max.x.try_into().unwrap_or(std::u8::MAX);
                            if width > self.width() {
                                self.set_width(width);
                            }
                            let mut fon_glyph = self.glyph_mut(cell);
                            let min = rect.min;
                            glyph.draw(|x, y, v| {
                                if let (Ok(x), Ok(y)) = (i32::try_from(x), i32::try_from(y)) {
                                    let x = x + min.x;
                                    let y = y + min.y;
                                    if let (Ok(x), Ok(y)) = (x.try_into(), y.try_into()) {
                                        fon_glyph.set(x, y, v > 0.5);
                                    }
                                }
                            });
                        }
                    }
                }
            }
        }
        Ok(())
    }

    pub(crate) fn from_reader<T: Read>(reader: T) -> Result<Self, Error> {
        let mut reader = reader;
        debug!("read page header");

        let next = reader.read_u32()?;
        debug!("  next = {} {0:#x}", next);

        let max_width = reader.read_u16()?;
        debug!("  width = {} {0:#x}", max_width);
        let max_width = u8::try_from(max_width).map_err(|_err| {
            Error::IncoherentHeader(format!(
                "width = {} (should be in <= {})",
                max_width,
                std::u8::MAX
            ))
        })?;

        let height = reader.read_u16()?;
        debug!("  height = {} {0:#x}", height);
        {
            let column_size = (height + 15) / 16;
            let char_size = column_size * u16::from(max_width);
            if next > u32::from(char_size) * 0x100 {
                return Err(Error::IncoherentHeader(format!(
                    "width = {}, height = {}, next = {} (should be <= {})",
                    max_width,
                    height,
                    next,
                    u32::from(char_size) * 0x100
                )));
            }
            if next > 0 && next % u32::from(char_size) != 0 {
                return Err(Error::IncoherentHeader(format!(
                    "width = {}, height = {}, next = {} (should be a multiple of {})",
                    max_width, height, next, char_size
                )));
            }
        }

        let char_size = reader.read_u16()?;
        debug!("  char size = {} {0:#x}", char_size);
        {
            let column_size = (height + 15) / 16;
            if char_size != column_size * u16::from(max_width) {
                return Err(Error::IncoherentHeader(format!(
                    "width = {}, height = {}, char size = {} (should be {})",
                    max_width,
                    height,
                    char_size,
                    char_size * column_size
                )));
            }
        }

        let column_size = reader.read_u16()?;
        debug!("  column size = {} {0:#x}", column_size);
        if column_size != (height + 15) / 16 {
            return Err(Error::IncoherentHeader(format!(
                "height = {}, column size = {} (should be {})",
                height,
                column_size,
                (height + 15) / 16
            )));
        }

        let mut indirect_table = [0; 0x100];
        reader.read_exact(&mut indirect_table)?;
        debug!("  indirect table = {:?}", indirect_table);
        if let Some((idx, val)) = indirect_table
            .iter()
            .copied()
            .enumerate()
            .find(|(idx, val)| usize::from(*val) > *idx)
        {
            let msg = format!("indirect[{}] = {} (should be < {0})", idx, val);
            return Err(Error::IncoherentHeader(msg));
        }

        let mut advances = [0; 0x100];
        reader.read_exact(&mut advances)?;
        debug!("  widths = {:?}", advances);

        let width = if next == 0 {
            0
        } else {
            debug_assert!(column_size != 0);
            checked_cast!(next / u32::from(column_size), u16)
        };
        let mut image = Image::from_reader(reader, width, height)?;
        image.set_width(u16::from(max_width) * 0x100);

        let char_size = char_size.into();
        let s = image.as_mut_slice();
        for cell in 0..=std::u8::MAX {
            let cell = usize::from(std::u8::MAX - cell);
            let slot = usize::from(indirect_table[cell]);
            debug_assert!(slot <= cell);
            debug!("read cell {} from slot {}", cell, slot);
            if cell != slot {
                for i in 0..char_size {
                    s[cell * char_size + i] = s[slot * char_size + i];
                }
            }
        }

        Ok(Self { image, advances })
    }

    #[must_use]
    fn count_uniq_glyph(&self) -> usize {
        let mut count = 0;
        for cur_idx in 0..=std::u8::MAX {
            let cur_glyph = self.glyph(cur_idx);
            if !(0..cur_idx).any(|prev_idx| cur_glyph.as_slice() == self.glyph(prev_idx).as_slice())
            {
                count += 1;
            }
        }
        count
    }

    #[must_use]
    pub(crate) fn size(&self) -> usize {
        let width = usize::from(self.width());
        let column_size = usize::from(self.image.column_size());
        let char_size = column_size * width * 2;
        0x20c + self.count_uniq_glyph() * char_size
    }

    pub(crate) fn to_writer<T: Write>(&self, writer: T) -> io::Result<()> {
        let mut writer = writer;

        let max_width = self.width();
        let mut indirect_table = [0_u8; 0x100];
        let mut indirect_max: u16 = 0; // up to 0x100
        {
            for cur_idx in 0..=std::u8::MAX {
                let cur_glyph = self.glyph(cur_idx);
                if let Some(prev_idx) = (0..cur_idx)
                    .find(|prev_idx| cur_glyph.as_slice() == self.glyph(*prev_idx).as_slice())
                {
                    debug!("cell {} is similar to {}", cur_idx, prev_idx);
                    indirect_table[usize::from(cur_idx)] = indirect_table[usize::from(prev_idx)];
                } else {
                    debug!("cell {} will be stored to slot {}", cur_idx, indirect_max);
                    #[allow(clippy::cast_possible_truncation)]
                    {
                        debug_assert!(indirect_max <= std::u8::MAX.into());
                        indirect_table[usize::from(cur_idx)] = indirect_max as u8;
                    }
                    indirect_max += 1;
                }
            }
        }
        let height = self.image.height();
        let column_size = self.image.column_size();
        let char_size = column_size * u16::from(max_width);
        let next = u32::from(indirect_max) * u32::from(char_size);
        debug!("write page header");
        debug!("  next = {} {0:#x}", next);
        writer.write_u32(next)?;

        debug!("  width = {} {0:#x}", max_width);
        writer.write_u16(u16::from(max_width))?;

        debug!("  height = {} {0:#x}", height);
        writer.write_u16(height)?;

        debug!("  char size = {} {0:#x}", char_size);
        writer.write_u16(char_size)?;

        debug!("  column size = {} {0:#x}", column_size);
        writer.write_u16(column_size)?;

        debug!("  indirect table = {:?}", indirect_table);
        writer.write_all(&indirect_table)?;

        debug!("  widths = {:?}", &self.advances);
        writer.write_all(&self.advances)?;

        let char_size = char_size.into();
        let mut slot = 0;
        for cell in 0..=std::u8::MAX {
            let cell = usize::from(cell);
            if indirect_table[cell] == slot {
                debug!("write cell {} to slot {}", cell, slot);
                for word in &self.image.as_slice()[cell * char_size..][..char_size] {
                    writer.write_u16(*word)?;
                }
                slot += 1;
            }
        }

        Ok(())
    }
}

impl Default for Page {
    fn default() -> Self {
        let image = Image::default();
        let advances = [0; 0x100];
        Self { image, advances }
    }
}
