mod glyph;
mod page;

pub use self::glyph::{Glyph, Mut as GlyphMut};
pub use self::page::Page;
use crate::{Error, Version};
use log::debug;
use rusttype::{Font as SystemFont, Scale};
use std::collections::{BTreeMap, HashSet};
use std::io::{self, Read, Write};

pub fn u16_from_f32(v: f32) -> Option<u16> {
    let v = v.round();
    #[allow(clippy::cast_possible_truncation)]
    #[allow(clippy::cast_sign_loss)]
    if (0.0..=65535.0).contains(&v) {
        Some(v as u16)
    } else {
        None
    }
}

/// The rendered font is too big
#[derive(Clone, Debug, Default, Eq, PartialEq)]
pub struct TooBig;

/// IML font
#[derive(Clone, Debug, Default, Eq, PartialEq)]
pub struct Font {
    /// Version of the font
    pub version: Version,
    /// Pages of the font stored by row
    pub pages: BTreeMap<u8, Page>,
}

impl Font {
    /// Expected magic bytes
    pub const MAGIC: [u8; 4] = *b"FONT";

    /// Import glyphs from a system font
    ///
    /// # Errors
    ///
    /// Returns an error if the rendered font is too large to fit in an IML font.
    pub fn import_system_font<T>(
        &mut self,
        font: &SystemFont,
        scale: Scale,
        codepoints: T,
    ) -> Result<(), TooBig>
    where
        T: IntoIterator<Item = char>,
    {
        fn import<T: Iterator<Item = u8>>(
            font: &SystemFont,
            scale: Scale,
            height: u16,
            pages: &mut BTreeMap<u8, Page>,
            row: u8,
            cells: T,
        ) -> Result<(), TooBig> {
            pages
                .entry(row)
                .or_insert_with(|| {
                    debug!("append page {}", row);
                    Page::new(0, height)
                })
                .import_system_font(font, scale, row, cells)
        }

        let v_metrics = font.v_metrics(scale);
        debug!(
            "ascend = {}, descent = {}",
            v_metrics.ascent, v_metrics.descent
        );
        let height = u16_from_f32(v_metrics.ascent - v_metrics.descent).ok_or(TooBig)?;
        let mut last_row = None;
        let mut cells = HashSet::new();
        for c in codepoints {
            let c = (c as u32).to_le_bytes();
            let cell = c[0];
            let row = c[1];
            if last_row.unwrap_or(row) == row {
                cells.insert(cell);
            } else if let Some(row) = last_row {
                import(font, scale, height, &mut self.pages, row, cells.drain())?;
            }
            last_row = Some(row);
        }
        if let Some(row) = last_row {
            import(font, scale, height, &mut self.pages, row, cells.into_iter())?;
        }
        Ok(())
    }

    pub(crate) fn from_reader<T: Read>(read: T, version: Version) -> Result<Self, Error> {
        let mut read = read;
        let mut pages = BTreeMap::new();
        {
            let mut page_head = [0; 4];
            read.read_exact(&mut page_head)?;
            let (plane, group, cell, row) =
                (page_head[0], page_head[1], page_head[2], page_head[3]);
            if plane != 0 || group != 0 {
                unimplemented!("plane {}, group {}", plane, group)
            }
            if cell != 0 {
                let msg = format!("cell = {} (should be 0)", cell);
                return Err(Error::IncoherentHeader(msg));
            }
            pages.insert(row, Page::from_reader(&mut read)?);
        }
        loop {
            let mut page_head = [0; 4];
            read.read_exact(&mut page_head)?;
            let (plane, group, cell, row) =
                (page_head[0], page_head[1], page_head[2], page_head[3]);
            if plane != 0 || group != 0 {
                unimplemented!("plane {}, group {}", plane, group)
            }
            if cell != 0 {
                let msg = format!("cell = {} (should be 0)", cell);
                return Err(Error::IncoherentHeader(msg));
            }
            if row == 0 {
                return Ok(Self { version, pages });
            }
            if pages.contains_key(&row) {
                return Err(Error::IncoherentHeader(format!("Duplicate page {}", row)));
            }
            pages.insert(row, Page::from_reader(&mut read)?);
        }
    }

    /// Remove empty pages
    pub fn pack(&mut self) {
        self.pages.retain(|row, page| {
            if page.width() > 0 {
                true
            } else {
                debug!("drop page {}", row);
                false
            }
        });
    }

    #[must_use]
    pub(crate) fn size(&self) -> usize {
        self.pages
            .iter()
            .map(|(_row, page)| 4 + page.size())
            .sum::<usize>()
            + 4
    }

    pub(crate) fn to_writer<T: Write>(&self, writer: T) -> io::Result<()> {
        let mut writer = writer;
        for (row, page) in &self.pages {
            let head = [0, 0, 0, *row];
            writer.write_all(&head)?;
            page.to_writer(&mut writer)?;
        }
        writer.write_all(&[0, 0, 0, 0])
    }
}

impl From<Version> for Font {
    #[inline]
    #[must_use]
    fn from(version: Version) -> Self {
        let mut pages = BTreeMap::new();
        pages.insert(0, Page::default());
        Self { version, pages }
    }
}
