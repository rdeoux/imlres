extern crate imlres;
extern crate rusttype;
extern crate simple_logger;

use imlres::{Font, Res, Version};
use log::info;
use rusttype::{Font as SystemFont, Scale};
use simple_logger::SimpleLogger;
use std::process::Command;
use tempfile::NamedTempFile;

const SIZE_PT: f32 = 12.0;
const SIZE_PX: f32 = SIZE_PT * 254.0 / 90.0;

#[test]
fn roboto() {
    SimpleLogger::new().init().ok();

    info!("load Roboto font from TTF");
    let ttf = SystemFont::try_from_bytes(include_bytes!("Roboto-Regular.ttf")).unwrap();

    info!("select size {}pt ({}px)", SIZE_PT, SIZE_PX);
    let scale = Scale::uniform(SIZE_PX);

    info!("create IML font \u{201c}Roboto12\u{201d}");
    let mut version = Version::default();
    version[..8].copy_from_slice(b"Roboto12");
    let mut fon = Font::from(version);

    info!("import glyphs");
    fon.import_system_font(&ttf, scale, '\0'..='\u{ffff}')
        .unwrap();

    info!("save IML font");
    let bytes = Res::Font(fon.clone()).to_bytes();

    info!("reload IML font");
    if let Res::Font(fon2) = Res::from_bytes(&bytes).unwrap() {
        info!("compare fonts");
        assert_eq!(fon.version, fon2.version);
        assert_eq!(fon.pages.len(), fon2.pages.len());
        for (page, page2) in fon.pages.iter().zip(fon2.pages.iter()) {
            assert_eq!(page.0, page2.0);
            info!("compare page {}", page.0);
            let (page, page2) = (page.1, page2.1);
            for cell in 0..=std::u8::MAX {
                info!("  compare cell {}", cell);
                assert_eq!(page.glyph(cell).as_slice(), page2.glyph(cell).as_slice())
            }
            assert_eq!(page, page2);
        }
        assert_eq!(fon, fon2);
    } else {
        panic!()
    }
}

#[test]
fn cli() {
    SimpleLogger::new().init().ok();

    let imlres = env!("CARGO_BIN_EXE_imlres");
    let iml_font_file = NamedTempFile::new().unwrap();
    let iml_font_path = iml_font_file.path();
    let system_font_path = "tests/Roboto-Regular.ttf";

    info!("Create empty font “Roboto12”");
    assert!(Command::new(imlres)
        .arg("font")
        .arg(&iml_font_path)
        .args(&["new", "Roboto12"])
        .status()
        .unwrap()
        .success());

    info!("Add digits from “{}” (12pt)", system_font_path);
    for digit in 0..=9 {
        assert!(Command::new(imlres)
            .arg("font")
            .arg(&iml_font_path)
            .args(&["add", system_font_path, "12pt", &format!("0x3{}", digit)])
            .status()
            .unwrap()
            .success())
    }

    info!("List glyphs");
    let output = String::from_utf8(
        Command::new(imlres)
            .arg("font")
            .arg(&iml_font_path)
            .arg("ls")
            .output()
            .unwrap()
            .stdout,
    );
    assert_eq!(
        output,
        Ok("48
49
50
51
52
53
54
55
56
57
"
        .to_string())
    );

    info!("Show digit “4”");
    let output = String::from_utf8(
        Command::new(imlres)
            .arg("font")
            .arg(&iml_font_path)
            .args(&["cat", "0x34"])
            .output()
            .unwrap()
            .stdout,
    );
    assert_eq!(
        output,
        Ok("
         
    ⢀⣤⡄  
   ⢠⡾⣿⡇  
  ⣰⡟ ⣿⡇  
⢀⣼⠏  ⣿⡇  
⠘⠛⠛⠛⠛⣿⡟⠛ 
     ⠿⠇  
         
         
"[1..]
            .to_string())
    );

    info!("Remove digit “4”");
    assert!(Command::new(imlres)
        .arg("font")
        .arg(&iml_font_path)
        .args(&["rm", "0x34"])
        .status()
        .unwrap()
        .success());

    info!("List glyphs");
    let output = String::from_utf8(
        Command::new(imlres)
            .arg("font")
            .arg(&iml_font_path)
            .arg("ls")
            .output()
            .unwrap()
            .stdout,
    );
    assert_eq!(
        output,
        Ok("48
49
50
51
53
54
55
56
57
"
        .to_string())
    );

    info!(
        "Create font “Roboto12” from {} 12pt (full)",
        system_font_path
    );
    assert!(Command::new(imlres)
        .args(["-vv", "font"])
        .arg(&iml_font_path)
        .args(&["new", "Roboto12", &system_font_path, "12pt"])
        .status()
        .unwrap()
        .success());

    info!("Show symbol “Ѽ” (Cyrillic Capital Letter Omega with Titlo)");
    let output = String::from_utf8(
        Command::new(imlres)
            .arg("font")
            .arg(&iml_font_path)
            .args(&["cat", "0x47c"])
            .output()
            .unwrap()
            .stdout,
    );
    assert_eq!(
        output,
        Ok("
    ⣰⠞⠳⠦⣤⡄        
  ⣀⣠ ⢀⡿  ⣄⡀       
 ⣸⡟⠉     ⠉⣿⡆      
 ⣿⡇  ⢀⣀   ⢹⡇      
 ⣿⡇  ⢸⣿   ⢸⡇      
 ⢿⣇  ⢸⣿⡀  ⣾⡇      
 ⠈⠻⠷⠶⠟⠙⠻⠶⠾⠛       
                  
                  
"[1..]
            .to_string())
    );
}
