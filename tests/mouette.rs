extern crate imlres;

use imlres::Res;
use log::info;
use simple_logger::SimpleLogger;

#[test]
fn mouette() {
    const BMP_BYTES: &[u8] = include_bytes!("mouette.bmp");
    const LGO_BYTES: &[u8] = include_bytes!("mouette.lgo");

    SimpleLogger::new().init().unwrap();

    info!("load IML logo");
    if let Res::Logo(logo) = Res::from_bytes(LGO_BYTES).unwrap() {
        info!("load bitmap");
        let bmp = image::load_from_memory(BMP_BYTES).unwrap().into_luma8();

        info!("compare images");
        assert_eq!(bmp.width(), logo.width().into());
        assert_eq!(bmp.height(), logo.height().into());
        for x in 0..logo.width() {
            for y in 0..logo.height() {
                assert_eq!(
                    bmp.get_pixel(x.into(), y.into()).0[0] < 0x80,
                    logo.get(x, y).unwrap()
                )
            }
        }

        info!("save IML logo");
        let bytes = Res::Logo(logo).to_bytes();
        assert_eq!(bytes, LGO_BYTES);
    } else {
        panic!()
    }
}
